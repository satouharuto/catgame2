﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ScoreDirector : MonoBehaviour
{
    GameObject pointText;
    int point = 0;

    // Start is called before the first frame update
    void Start()
    {
        this.pointText = GameObject.Find("Score");
    }
    public void  Getpoint()
    {
        this.pointText.GetComponent<Text>().text = this.point.ToString() + "point";
        
        this.point += 100;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
